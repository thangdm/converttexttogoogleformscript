﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ConvertGForm
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = args[0];
            string formName = filePath.Split(new char[]{'/','\\'}, StringSplitOptions.RemoveEmptyEntries).Last();;
            for(int i=formName.Length-1;i>=0;i--)
                if(formName[i]=='.') { formName = formName.Substring(0,i); break; }
            string question = "";
            int point = 1;
            List<String> answers = new List<string>();
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("var form = FormApp.create('"+formName+"');");
            sb.AppendLine("form.setIsQuiz(true);");
            sb.AppendLine("var item;");

            foreach(string lines in File.ReadLines(filePath)) {
                string line = lines.Trim();
                
                if(line.IndexOf(". ")==1 && "ABCDEFGHabcdefgh".IndexOf(line[0])>=0) {
                    answers.Add(line);
                }
                else if (answers.Count>0) {
                    if(!string.IsNullOrEmpty(question))
                    {
                        sb.AppendLine("item = form.addCheckboxItem();");
                        //sb.AppendLine("item.setTitle('"+question.Replace(Environment.NewLine,"\\r\\n")+"');");
                        sb.AppendLine("item.setTitle('"+question.Substring(0,question.IndexOf(Environment.NewLine))+"');");
                        sb.AppendLine("item.setHelpText('"+question.Substring(question.IndexOf(Environment.NewLine)).Replace(Environment.NewLine,"\\n")+"');");
                        sb.AppendLine("item.setChoices([");
                        for(int i=0;i<answers.Count;i++) {
                            sb.AppendLine("item.createChoice('"+answers[i]+"')"+(i==answers.Count-1?"":","));
                        }
                        sb.AppendLine("]);");
                        sb.AppendLine("item.setPoints("+point.ToString()+");");
                        question = "";
                        point = 1;
                        answers.Clear();
                    }
                }
                else if (string.IsNullOrEmpty(question) && !line.StartsWith("Question"))
                    continue;
                else {
                    if(line.StartsWith("Question")) {
                        try {
                            point = int.Parse((double.Parse(line.Substring(line.IndexOf("[")+1,line.IndexOf("]")-line.IndexOf("[")-1).Replace(",","."))*2).ToString());
                        } catch{ point = 1;}
                    }
                    question += (question==""?"":Environment.NewLine)+line;
                }
            }
            sb.AppendLine("Logger.log('Published URL: ' + form.getPublishedUrl());\nLogger.log('Editor URL: ' + form.getEditUrl());");
            File.WriteAllText(filePath+".script",sb.ToString());
        }
    }
}
